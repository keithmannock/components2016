Source code examples for the

* Component Based Software Development, and
* Enterprise Computing

modules at Birkbeck College, Dept of Computer Science.

Spring term 2016

The folders contain the following information

* cargo-tracker - The JEE Blueprint example that has replace the "PetStore" as one of the main examples of JEE design - https://cargotracker.java.net
* firstcup - The JEE example code 
* tutorial - The "First Cup" tutorial document that all students should complete regardless of coursework type
